namespace WebApplication1.Infrastructure.Tasks
{
	public interface IRunAtStartup
	{
		void Execute();
	}
}