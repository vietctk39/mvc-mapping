namespace WebApplication1.Infrastructure.Tasks
{
	public interface IRunOnEachRequest
	{
		void Execute();
	}
}