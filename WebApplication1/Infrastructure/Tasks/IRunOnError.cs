namespace WebApplication1.Infrastructure.Tasks
{
	public interface IRunOnError
	{
		void Execute();
	}
}