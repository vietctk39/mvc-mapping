namespace WebApplication1.Infrastructure.Tasks
{
	public interface IRunAfterEachRequest
	{
		void Execute();
	}
}