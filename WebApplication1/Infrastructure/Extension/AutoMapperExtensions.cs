﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace WebApplication1.Infrastructure.Extension
{
    public static class AutoMapperExtensions
    {
        public static TDest MapTo<TSource, TDest>(this TSource source)
        {
            return Mapper.Map<TSource, TDest>(source);
        }
    }
}