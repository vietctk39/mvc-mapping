﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap.Configuration.DSL;

namespace WebApplication1.Infrastructure
{
    public class ControllerRegistry:Registry
    {
        public ControllerRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.With(new ControllerConvention());
            });
        }
    }
}