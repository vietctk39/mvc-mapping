﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Infrastructure.ModelMetadata.Filter
{
    public class ReadOnlyTemplatesSelectorFilter: IModelMetadataFilter
    {
        public void TransformMetadata(System.Web.Mvc.ModelMetadata metadata, IEnumerable<Attribute> attributes)
        {
            if (metadata.IsReadOnly && string.IsNullOrEmpty(metadata.DataTypeName))
            {
                metadata.DataTypeName ="ReadOnly";
            }
        }
    }
}