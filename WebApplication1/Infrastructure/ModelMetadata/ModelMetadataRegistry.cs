﻿using System.Web.Mvc;
using StructureMap.Configuration.DSL;

namespace WebApplication1.Infrastructure.ModelMetadata
{
    public class ModelMetadataRegistry:Registry
    {
        public ModelMetadataRegistry()
        {
            For<ModelMetadataProvider>().Use<ExtensibleModelMetadataProvider>();
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.AddAllTypesOf<IModelMetadataFilter>();
            });
        }
    }
}