﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Mvc;
using WebApplication1.Filters;

namespace WebApplication1.Infrastructure
{
    [IssueTypeSelectListPopulator, UserSelectListPopulator]
    public abstract class FailTrackerController:Controller
    {
        protected ActionResult RedirectToAction<TController>(Expression<Action<TController>> action)
            where TController : Controller
        {
            return ControllerExtensions.RedirectToAction(this, action);
        }
    }
}