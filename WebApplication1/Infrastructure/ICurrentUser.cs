﻿using WebApplication1.Models;

namespace WebApplication1.Infrastructure
{
    public interface ICurrentUser
    {
        ApplicationUser User { get; }
    }
}