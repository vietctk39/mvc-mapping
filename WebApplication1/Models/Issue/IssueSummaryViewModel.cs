﻿using System;
using WebApplication1.Domain;
using WebApplication1.Infrastructure.Mapping;

namespace WebApplication1.Models.Issue
{
    public class IssueSummaryViewModel : IMapFrom<Domain.Issue>
    {
        public int IssueId { get; set; }
        public string Subject { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatorUserName { get; set; }
        public IssueType IssueType { get; set; }
        public string AssignedToUserName { get; set; }
    }
}