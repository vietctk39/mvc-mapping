﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Domain;
using WebApplication1.Infrastructure.Mapping;

namespace WebApplication1.Models.Issue
{
    public class IssueDetailsViewModel : IMapFrom<Domain.Issue>
    {
        public int IssueId { get; set; }
        public string Subject { get; set; }
        public string CreatorUserName { get; set; }
        public string AssignedToUserName { get; set; }
        public DateTime CreatedAt { get; set; }
        public IssueType IssueType { get; set; }
        public string Body { get; set; }
    }
}