﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using WebApplication1.Domain;
using WebApplication1.Filters;
using WebApplication1.Infrastructure.Mapping;

namespace WebApplication1.Models.Issue
{
    public class NewIssueForm:  IHaveCustomMappings
    {
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }

        public IssueType IssueType { get; set; }
        //public SelectListItem[] AvailableIssueTypes { get; set; }

        [Display(Name = "Assigned To")]
        public string AssignedToUserId { get; set; }
       // public SelectListItem[] AvailableUsers { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<NewIssueForm, Domain.Issue>()
                .ForMember(x => x.CreatedAt, o => o.UseValue(DateTime.Now));
        }
    }
}