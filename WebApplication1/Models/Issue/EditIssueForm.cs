﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebApplication1.Domain;
using WebApplication1.Filters;
using WebApplication1.Infrastructure.Mapping;

namespace WebApplication1.Models.Issue
{
    public class EditIssueForm : IMapFrom<Domain.Issue>
    {
        [HiddenInput]
        public int IssueId { get; set; }
       
        [ReadOnly(true)]
        public string CreatorUserName { get; set; }
        public string Subject { get; set; }
       

        public IssueType IssueType { get; set; }
        //public SelectListItem[] AvailableIssueTypes { get; set; }

        [Display(Name = "Assigned To")]
        public string AssignedToId { get; set; }
        //public SelectListItem[] AvailableUsers { get; set; }
        [Required]
        public string Body { get; set; }
    }
}