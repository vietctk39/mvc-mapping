﻿using System.Linq;
using AutoMapper;
using WebApplication1.Domain;
using WebApplication1.Infrastructure.Mapping;

namespace WebApplication1.Models.Issue
{
    public class AssignmentStatsViewModel : IHaveCustomMappings
    {
        public string UserName { get; set; }
        public int Enhancements { get; set; }
        public int Bugs { get; set; }
        public int Support { get; set; }
        public int Other { get; set; }
        public void CreateMappings(IConfiguration configuration)
        {
            Mapper.CreateMap<ApplicationUser, AssignmentStatsViewModel>()
                .ForMember(m => m.Enhancements, o => o.MapFrom(u => u.Assignments.Count(i => i.IssueType == IssueType.Enhancement)))
                .ForMember(m => m.Bugs, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Bug)))
                .ForMember(m => m.Support, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Support)))
                .ForMember(m => m.Other, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Other)));
        }
    }
}