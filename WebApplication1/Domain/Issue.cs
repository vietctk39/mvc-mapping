﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Domain
{
    public class Issue
    {
        public int IssueId { get; set; }

        public string CreatorId { get; set; }
        [ForeignKey("CreatorId")]
        public ApplicationUser Creator { get; set; }
        public string AssignedToId { get; set; }
        [ForeignKey("AssignedToId")]

        public ApplicationUser AssignedTo { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public DateTime CreatedAt { get; set; }
        public IssueType IssueType { get; set; }
    }
}