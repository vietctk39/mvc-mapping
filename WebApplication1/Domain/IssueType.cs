﻿namespace WebApplication1.Domain
{
    public enum IssueType
    {
        Enhancement,
        Bug,
        Support,
        Other
    }
}