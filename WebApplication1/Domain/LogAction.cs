﻿using System;
using WebApplication1.Models;

namespace WebApplication1.Domain
{
    public class LogAction
    {
        public int LogActionId { get; set; }

        public DateTime PerformedAt { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }
        public string PerformedById { get; set; }

        public ApplicationUser PerformedBy { get; set; }

        public string Description { get; set; }
    }
}