﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using WebApplication1.Infrastructure.Mapping;
using WebApplication1.Infrastructure.Tasks;

namespace WebApplication1
{
    public class AutoMapperConfig:IRunAtInit
    {
        public void Execute()
        {
            //Mapper.CreateMap<Issue, IssueSummaryViewModel>();

            //Mapper.CreateMap<Issue, IssueDetailsViewModel>();

            //Mapper.CreateMap<Issue, EditIssueForm>();

            //Mapper.CreateMap<ApplicationUser, AssignmentStatsViewModel>()
            //    .ForMember(m => m.Enhancements, o => o.MapFrom(u => u.Assignments.Count(i => i.IssueType == IssueType.Enhancement)))
            //    .ForMember(m => m.Bugs, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Bug)))
            //    .ForMember(m => m.Support, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Support)))
            //    .ForMember(m => m.Other, o => o.MapFrom(u => u.Assignments.Count(x => x.IssueType == IssueType.Other)));

            var types = Assembly.GetExecutingAssembly().GetExportedTypes();
            LoadStandardMappings(types);
            LoadCustomMappings(types);
        }

        private static void LoadCustomMappings(IEnumerable<Type> types)
        {
            //var maps = (from t in types
            //    from i in t.GetInterfaces()
            //    where typeof(IHaveCustomMappings).IsAssignableFrom(t) &&
            //          !t.IsAbstract &&
            //          !t.IsInterface
            //    select (IHaveCustomMappings)Activator.CreateInstance(t)).ToArray();

            var maps = (types.SelectMany(t => t.GetInterfaces(), (t, i) => new {t, i})
                .Where(@t1 =>
                    typeof(IHaveCustomMappings).IsAssignableFrom(@t1.t) && !@t1.t.IsAbstract && !@t1.t.IsInterface)
                .Select(@t1 => (IHaveCustomMappings) Activator.CreateInstance(@t1.t))).ToArray();
            foreach (var map in maps)
            {
                map.CreateMappings(Mapper.Configuration);
            }
        }

        private static void LoadStandardMappings(IEnumerable<Type> types)
        {
            //var maps = (from t in types
            //    from i in t.GetInterfaces()
            //    where i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>) &&
            //          !t.IsAbstract &&
            //          !t.IsInterface
            //    select new
            //    {
            //        Source = i.GetGenericArguments()[0],
            //        Destination = t
            //    }).ToArray();

            var maps = (types.SelectMany(t => t.GetInterfaces(), (t, i) => new { t, i })
                .Where(@t1 =>
                    @t1.i.IsGenericType && @t1.i.GetGenericTypeDefinition() == typeof(IMapFrom<>) &&
                    !@t1.t.IsAbstract && !@t1.t.IsInterface)
                .Select(@t1 => new { Source = @t1.i.GetGenericArguments()[0], Destination = @t1.t })).ToArray();

            foreach (var map in maps)
            {
                Mapper.CreateMap(map.Source, map.Destination);
            }
        }
    }
}