﻿using AutoMapper.QueryableExtensions;
using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Web.Mvc;
using WebApplication1.Domain;
using WebApplication1.Filters;
using WebApplication1.Infrastructure;
using WebApplication1.Infrastructure.Alerts;
using WebApplication1.Infrastructure.Extension;
using WebApplication1.Models;
using WebApplication1.Models.Issue;

namespace WebApplication1.Controllers
{
    public class IssueController : FailTrackerController
    {
        private readonly ApplicationDbContext _context;
        private readonly ICurrentUser _currentUser;

        // GET: Issue
        public IssueController(ApplicationDbContext context, ICurrentUser currentUser)
        {
            _context = context;
            _currentUser = currentUser;
        }
        //private SelectListItem[] GetAvailableUsers()
        //{
        //    return _context.Users.Select(u => new SelectListItem { Text = u.UserName, Value = u.Id }).ToArray();
        //}

        //private SelectListItem[] GetAvailableIssueTypes()
        //{
        //    return Enum.GetValues(typeof(IssueType))
        //        .Cast<IssueType>()
        //        .Select(t => new SelectListItem { Text = t.ToString(), Value = t.ToString() })
        //        .ToArray();
        //}
        [ChildActionOnly]
        public ActionResult YourIssuesWidget()
        {
            //var models = _context.Issues.Where(i => i.AssignedToUserName.Id == _currentUser.User.Id)
            //    .Select(i => new IssueSummaryViewModel
            //    {
            //        IssueId = i.IssueId,
            //        Subject = i.Subject,
            //        IssueType = i.IssueType,
            //        CreatedAt = i.CreatedAt,
            //        CreatorUserName = i.CreatorUserName.UserName
            //    });
            var models2 = _context.Issues.Where(i => i.AssignedTo.Id == _currentUser.User.Id).Project().To<IssueSummaryViewModel>().ToArray();
            return PartialView(models2);
        }
        [ChildActionOnly]
        public ActionResult CreatedByYouWidget()
        {
            //var models = _context.Issues.Where(i => i.CreatorUserName.Id == _currentUser.User.Id)
            //    .Select(i => new IssueSummaryViewModel
            //    {
            //        IssueId = i.IssueId,
            //        Subject = i.Subject,
            //        IssueType = i.IssueType,
            //        CreatedAt = i.CreatedAt,
            //        AssignedToUserName = i.AssignedToUserName.UserName
            //    });
            var models2 = _context.Issues.Where(i => i.Creator.Id == _currentUser.User.Id).Project().To<IssueSummaryViewModel>().ToArray();
            return PartialView(models2);
        }
        [ChildActionOnly]
        public ActionResult AssignmentStatsWidget()
        {
            var models2 = _context.Users.Project().To<AssignmentStatsViewModel>().ToArray();
            return PartialView(models2);
        }
        public ActionResult New()
        {
            var form = new NewIssueForm
            {
                //AvailableUsers = GetAvailableUsers(),
                //AvailableIssueTypes = GetAvailableIssueTypes()
            };
            return View(form);
        }
        [HttpPost, ValidateAntiForgeryToken,Log("Created Issue")]
        public ActionResult New(NewIssueForm form)
        {
            if (!ModelState.IsValid)
            {
                //form.AvailableUsers = GetAvailableUsers();
                //form.AvailableIssueTypes = GetAvailableIssueTypes();
                return View(form);
            }
            var assignedToUser = _context.Users.Single(u => u.Id == form.AssignedToUserId);
            var entity = form.MapTo<NewIssueForm, Issue>();
            entity.Creator = _currentUser.User;
            entity.AssignedTo = assignedToUser;
            _context.Issues.Add(entity);
            //_context.Issues.Add(new Issue(_currentUser.User, assignedToUser, form.IssueType, form.Subject, form.Body));
            _context.SaveChanges();
            //return RedirectToAction("Index", "Home");
            return RedirectToAction<HomeController>(c => c.Index()).WithSuccess("Issue created!!");
        }

        [Log("View Issue {id}")]
        public ActionResult View(int id)
        {
            //var models = _context.Issues
            //    .Include(i => i.AssignedToUserName)
            //    .Include(i => i.CreatorUserName).Select(x => new IssueDetailsViewModel
            //    {
            //        IssueId = x.IssueId,
            //        Subject = x.Subject,
            //        CreatedAt = x.CreatedAt,
            //        AssignedToUserName = x.AssignedToUserName.UserName,
            //        CreatorUserName = x.CreatorUserName.UserName,
            //        IssueType = x.IssueType,
            //        Body = x.Body
            //    }).SingleOrDefault(i => i.IssueId == id);
            var models = _context.Issues.Project().To< IssueDetailsViewModel >().SingleOrDefault(i => i.IssueId == id);

            if (models == null)
            {
                return RedirectToAction<HomeController>(c => c.Index()).WithError("Can not fin issue..");
            }

            return View(models);
        }
        [Log("Started to edit issue {id}")]
        public ActionResult Edit(int id)
        {
            //var issue = _context.Issues
            //    .Include(i => i.AssignedToUserName)
            //    .Include(i => i.CreatorUserName)
            //    .SingleOrDefault(i => i.IssueId == id);

            //if (issue == null)
            //{
            //    throw new ApplicationException("Issue not found!");
            //}

            //return View(new EditIssueForm
            //{
            //    IssueId = issue.IssueId,
            //    Subject = issue.Subject,
            //    AssignedToId = issue.AssignedToUserName.Id,
            //    AvailableUsers = GetAvailableUsers(),
            //    CreatorUserName = issue.CreatorUserName.UserName,
            //    IssueType = issue.IssueType,
            //    AvailableIssueTypes = GetAvailableIssueTypes(),
            //    Body = issue.Body
            //});

            var models = _context.Issues.Project().To<EditIssueForm>().SingleOrDefault(i => i.IssueId == id);
            if (models == null)
            {
                return RedirectToAction<HomeController>(c => c.Index()).WithError("Can not fin issue..");
            }

            //models.AvailableUsers = GetAvailableUsers();
            //models.AvailableIssueTypes = GetAvailableIssueTypes();
            return View(models);
        }
        [HttpPost, Log("Saving changes")]
        public ActionResult Edit(EditIssueForm form)
        {
            if (!ModelState.IsValid)
            {
                //form.AvailableUsers = GetAvailableUsers();
                //form.AvailableIssueTypes = GetAvailableIssueTypes();
                return View(form);
            }

            var issue = _context.Issues.SingleOrDefault(i => i.IssueId == form.IssueId);

            if (issue == null)
            {
                return RedirectToAction<HomeController>(c => c.Index()).WithError("Can not fin issue..");
            }

            var assignedToUser = _context.Users.Single(u => u.Id == form.AssignedToId);

            issue.Subject = form.Subject;
            issue.AssignedTo = assignedToUser;
            issue.Body = form.Body;
            issue.IssueType = form.IssueType;

            //return RedirectToAction("View", new { id = form.IssueId });
            return this.RedirectToAction(c => c.View(form.IssueId)).WithSuccess("update ok");
        }




        [HttpPost, ValidateAntiForgeryToken,Log("Delete Issue {id}")]
        public ActionResult Delete(int id)
        {
            var issue = _context.Issues.Find(id);
            if (issue==null)
            {
                return RedirectToAction<HomeController>(c => c.Index()).WithError("Can not fin issue..");
            }
            _context.Issues.Remove(issue);
            _context.SaveChanges();
            //return RedirectToAction("Index", "Home");
            return RedirectToAction<HomeController>(c => c.Index()).WithSuccess("Delete success");
        }
    }
}