﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using StructureMap;
using StructureMap.TypeRules;
using WebApplication1.Infrastructure;
using WebApplication1.Infrastructure.ModelMetadata;
using WebApplication1.Infrastructure.Tasks;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DependencyResolver.SetResolver(new StructureMapDependencyResolver(() => Container ?? ObjectFactory.Container));
            ObjectFactory.Configure(cfg =>
            {
                cfg.AddRegistry(new StandardRegistry());
                cfg.AddRegistry(new ControllerRegistry());
                cfg.AddRegistry(new ActionFilterRegistry(() => Container?? ObjectFactory.Container));
                cfg.AddRegistry(new MVCRegistry());
                cfg.AddRegistry(new TaskRegistry());
                cfg.AddRegistry(new ModelMetadataRegistry());
                //cfg.Scan(s =>
                //{
                //    s.TheCallingAssembly();
                //    s.WithDefaultConventions();
                //    s.With(new ControllerConvention());
                //});
                //cfg.For<IFilterProvider>()
                //    .Use(new StructureMapFilterProvider(() => Container ?? ObjectFactory.Container));

                //cfg.SetAllProperties(x => 
                //    x.Matching(p=>
                //        p.DeclaringType.CanBeCastTo(typeof(ActionFilterAttribute)) &&
                //        p.DeclaringType.Namespace.StartsWith("FailTracker") &&
                //        !p.PropertyType.IsPrimitive &&
                //        p.PropertyType != typeof(string)));
            });
            using (var container = ObjectFactory.Container.GetNestedContainer())
            {
                foreach (var task in container.GetAllInstances<IRunAtInit>())
                {
                    task.Execute();
                }
                foreach (var task in container.GetAllInstances<IRunAtStartup>())
                {
                    task.Execute();
                }

            }
        }
        public IContainer Container
        {
            get => (IContainer)HttpContext.Current.Items["_Container"];
            set => HttpContext.Current.Items["_Container"] = value;
        }
        public void Application_BeginRequest()
        {
            Container = ObjectFactory.Container.GetNestedContainer();
            foreach (var task in Container.GetAllInstances<IRunOnEachRequest>())
            {
                task.Execute();
            }
        }

        public void Application_Error()
        {
            foreach (var task in Container.GetAllInstances<IRunOnError>())
            {
                task.Execute();
            }
        }
        public void Application_EndRequest()
        {
            try
            {
                foreach (var task in Container.GetAllInstances<IRunAfterEachRequest>())
                {
                    task.Execute();
                }
            }
            finally
            {
                Container.Dispose();
                Container = null;
            }
        }
    }
}
