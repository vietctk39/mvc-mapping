﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication1.Domain;
using WebApplication1.Infrastructure;
using WebApplication1.Models;

namespace WebApplication1.Filters
{
    public class LogAttribute : ActionFilterAttribute
    {

        private IDictionary<string, object> _parameter;
        public ICurrentUser CurrentUser { get; set; }


        public ApplicationDbContext Context { get; set; }
        public string Description { get; set; }

        public LogAttribute(string description)
        {
            Description = description;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _parameter = filterContext.ActionParameters;
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //var context = new ApplicationDbContext();
            //var userId = filterContext.HttpContext.User.Identity.GetUserId();
            //var user = Context.Users.Find(userId);
            var description = Description;
            foreach (var kvp in _parameter)
            {
                description = description.Replace("{" + kvp.Key + "}", kvp.Value.ToString());
            }

            Context.LogActions.Add(new LogAction
            {
                Action = filterContext.ActionDescriptor.ActionName,
                Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                Description = description,
                PerformedBy = CurrentUser.User,
                PerformedAt = DateTime.Now
            });
            Context.SaveChanges();
        }
    }
}